import React, {Component} from 'react';
import {Button, Select} from 'semantic-ui-react';
import './recherche.css';
class Recherche extends Component{
    state = {
        dpt : '',
        type: ''
    }
    onDptChange = (e, data)=>{
        this.setState({dpt: data.value})
    }
    onTypeChange = (e, data)=>{
        this.setState({type: data.value})
    }
    render(){
        const optionsDpt = [
            { value: "75", key:"75", text:"Paris"},
            { value: "78", key:"78", text:"Yvelines"},
            { value: "95", key:"95", text:"Val d'Oise"},
            { value: "91", key:"91", text:"Essonne"},
            { value: "77", key:"77", text:"Seine-et-Marne"},
            { value: "92", key:"92", text:"Haut de Seine"},
            { value: "93", key:"93", text:"Seine Saint Denis"},



        ];
        const optionsType = [
            { value: "cpam", key:"cpam", text:"Caisse primaire d'assurance maladie"},
            { value: "cci", key:"cci", text:"Chambre de commerce et d'industrie"},
            { value: "mairie", key:"mairie", text:"Mairie"}      
        ];
        return(
            <div className="recherche">
                <Select placeholder="Choisissez un département" onChange={this.onDptChange} options={optionsDpt} value={this.state.dpt}></Select>
                <Select placeholder="Choisissez une administration" onChange={this.onTypeChange} options={optionsType} value= {this.state.type}></Select>
                <Button primary onClick={()=>this.props.onSearch(this.state.dpt, this.state.type)}>Lancer la recherche</Button>
                <Button secondary onClick={this.props.onEmpty}>Vider la recherche</Button>
            </div>
        )
    }
}

export default Recherche;